package models

import play.api.db._
import play.api.libs.json.Json

import java.sql.Connection

case class RunwayIdent(
  ident: String,
  count: Int
)

object RunwayIdent {
  implicit val format = Json.format[RunwayIdent]

  def fetchMostCommon(conn: Connection): Seq[RunwayIdent] = {
    val rs = conn.createStatement.executeQuery(
      "SELECT le_ident, COUNT(*) AS count " +
      "FROM runways " +
      "GROUP BY le_ident " +
      "ORDER BY count DESC " +
      "LIMIT 10"
    )

    var res = List[RunwayIdent]()
    while (rs.next()) {
      res = RunwayIdent(rs.getString("le_ident"), rs.getInt("count")) :: res
    }

    res.reverse
  }
}
