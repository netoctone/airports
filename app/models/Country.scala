package models

import play.api.db._
import play.api.libs.json.Json

import java.sql.Connection

case class Country(
  id: Int,
  code: String,
  name: String,
  airports_count: Option[Int],
  surfaces: List[String]
)

object Country {
  implicit val format = Json.format[Country]

  def findLikeName(conn: Connection, name: String): Option[Country] = {
    val nameLower = name.toLowerCase

    val st = conn.prepareStatement(
      "SELECT id, code, name FROM countries " +
      s"WHERE LOWER(name) = ? OR LOWER(code) = ? OR LOWER(name) LIKE ? OR LOWER(keywords) LIKE ?"
    )
    st.setString(1, nameLower)
    st.setString(2, nameLower)
    st.setString(3, s"%${nameLower}%")
    st.setString(4, s"%${nameLower}%")

    var res = List[Country]()
    val rs = st.executeQuery()
    while (rs.next()) {
      res = Country(rs.getInt("id"), rs.getString("code"), rs.getString("name"), None, List[String]()) :: res
    }

    val country = res.find((country) => country.name.toLowerCase == nameLower || country.code.toLowerCase == nameLower)
    if (country.isDefined) {
      country
    } else {
      res.find((country) => true)
    }
  }

  def fetchOrderByAirports(conn: Connection): Seq[Country] = {
    val rs = conn.createStatement.executeQuery(
      "SELECT countries.id, countries.code, countries.name, COUNT(airports.id) AS airports_count " +
      "FROM countries LEFT JOIN airports ON countries.id = airports.country_id " +
      "GROUP BY countries.id " +
      "ORDER BY airports_count DESC"
    )

    var res = List[Country]()
    while (rs.next()) {
      res = Country(
        rs.getInt("countries.id"),
        rs.getString("countries.code"),
        rs.getString("countries.name"),
        Some(rs.getInt("airports_count")),
        List[String]()
      ) :: res
    }

    res.reverse
  }

  def fetchWithSurface(conn: Connection): Seq[Country] = {
    val rs = conn.createStatement.executeQuery(
      "SELECT countries.id, countries.code, countries.name, runways.surface " +
      "FROM countries " +
      "INNER JOIN airports ON countries.id = airports.country_id " +
      "INNER JOIN runways ON airports.id = runways.airport_id " +
      "GROUP BY countries.id, runways.surface"
    )

    var res = List[Country]()
    while (rs.next()) {
      val id = rs.getInt("countries.id")

      if (res.isEmpty || res.head.id != id) {
        res = Country(
          rs.getInt("countries.id"),
          rs.getString("countries.code"),
          rs.getString("countries.name"),
          None,
          List(rs.getString("runways.surface"))
        ) :: res
      } else {
        val head = res.head

        res = Country(
          head.id,
          head.code,
          head.name,
          None,
          rs.getString("runways.surface") :: head.surfaces
        ) :: res.tail
      }
    }

    res.reverse
  }
}
