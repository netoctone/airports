package models

import play.api.db._
import play.api.libs.json.Json

import java.sql.Connection

case class Runway(
  surface: String,
  le_ident: String
)

object Runway {
  implicit val format = Json.format[Runway]
}

case class Airport(
  id: Int,
  ident: String,
  type_field: String,
  name: String,
  runways: List[Runway]
)

object Airport {
  implicit val format = Json.format[Airport]

  val perPage = 50

  def fetchByCountry(conn: Connection, country: Int, page: Int): Seq[Airport] = {
    var res = List[Airport]()

    val rs = conn.createStatement.executeQuery(
      s"SELECT airports.id, airports.ident, airports.type, airports.name, runways.surface, runways.le_ident " +
      s"FROM airports LEFT JOIN runways ON airports.id = runways.airport_id WHERE airports.country_id = ${country} LIMIT ${perPage} OFFSET ${(page - 1) * perPage}"
    )

    while (rs.next()) {
      val id = rs.getInt("airports.id")

      if (res.isEmpty || res.head.id != id) {
        res = Airport(
          id,
          rs.getString("airports.ident"),
          rs.getString("airports.type"),
          rs.getString("airports.name"),
          List(Runway(rs.getString("runways.surface"), rs.getString("runways.le_ident")))
        ) :: res
      } else {
        val h = res.head
        res = Airport(
          h.id,
          h.ident,
          h.type_field,
          h.name,
          Runway(rs.getString("runways.surface"), rs.getString("runways.le_ident")) :: h.runways
        ) :: res.tail
      }
    }
    rs.close()

    res.reverse
  }
}
