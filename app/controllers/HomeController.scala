package controllers

import javax.inject._
import play.api.mvc._
import play.api.db._

import play.api.libs.json.Json

import models._

case class SearchResponse(
  country: Country,
  airports: Seq[Airport]
)

object SearchResponse {
  implicit val format = Json.format[SearchResponse]
}

case class ReportResponse(
  countriesTop: Seq[Country],
  countriesLow: Seq[Country],
  countriesWithSurface: Seq[Country],
  runways: Seq[RunwayIdent]
)

object ReportResponse {
  implicit val format = Json.format[ReportResponse]
}

@Singleton
class HomeController @Inject()(db: Database, cc: ControllerComponents) extends AbstractController(cc) {

  def index = Action {
    Ok(views.html.index())
  }

  def search(country: String, page: Int) = Action {
    db.withConnection((conn) => {
      Country.findLikeName(conn, country) match {
        case Some(rec) => Ok(Json.toJson(SearchResponse(
          rec,
          Airport.fetchByCountry(conn, rec.id, page)
        )))
        case _ => NotFound("")
      }
    })
  }

  def report = Action {
    db.withConnection((conn) => {
      val countries = Country.fetchOrderByAirports(conn)

      Ok(Json.toJson(ReportResponse(
        countries.take(10),
        countries.reverse.take(10),
        Country.fetchWithSurface(conn),
        RunwayIdent.fetchMostCommon(conn)
      )))
    })
  }

}
