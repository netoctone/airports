(function() {
  var app = angular.module('air', []);

  app.controller('PageCtrl', function($scope) {
    $scope.page = "query";
  });

  app.controller('QueryCtrl', function($scope, $http) {
    $scope.airports = null;

    $scope.noMore = false;
    $scope.nextPage = 2;
    $scope.lastSearch = null;
    $scope.fetching = false;

    $scope.fetch = function() {
      if ($scope.search.length < 2) {
        $scope.message = "Please enter at least 2 characters";
      } else {
        $scope.message = "Loading ...";
        $scope.airports = null;
        $scope.noMore = false;
        $scope.nextPage = 2;
        $scope.lastSearch = $scope.search;

        $http({
          method: "GET",
          url: "/search?country=" + $scope.search
        }).then(function (resp) {
          $scope.message = "Country: " + resp.data.country.name + " (" + resp.data.country.code + ")";
          $scope.airports = resp.data.airports;
        }).catch(function (resp) {
          $scope.message = "Not found";
        });
      }
    }

    $scope.fetchNext = function() {
      if ($scope.fetching) {
        return;
      }
      $scope.fetching = true;

      $http({
        method: "GET",
        url: "/search?country=" + $scope.lastSearch + "&page=" + $scope.nextPage
      }).then(function (resp) {
        if (resp.data.airports.length == 0) {
          $scope.noMore = true;
        }

        $scope.nextPage += 1;
        $scope.airports = $scope.airports.concat(resp.data.airports);
        $scope.fetching = false;
      }).catch(function (resp) {
        $scope.fetching = false;
      });
    }
  });

  app.controller('ReportCtrl', function($scope, $http) {
    $http({
      method: "GET",
      url: "/report"
    }).then(function (resp) {
      $scope.countriesTop = resp.data.countriesTop;
      $scope.countriesLow = resp.data.countriesLow;
      $scope.countriesWithSurface = resp.data.countriesWithSurface;
      $scope.runways = resp.data.runways;
    });
  });
})();
