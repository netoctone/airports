package populate

import play.api.db._
import play.api.db.evolutions._
import java.sql.Connection
import com.github.tototoshi.csv._

object Main extends App {

  val queryDrop = """
    DROP TABLE IF EXISTS runways;
    DROP TABLE IF EXISTS airports;
    DROP TABLE IF EXISTS countries;
  """

  val queryCreate = """
  CREATE TABLE countries (
    id INT NOT NULL AUTO_INCREMENT,
    code VARCHAR(8),
    name VARCHAR(50),
    continent VARCHAR(8),
    wikipedia_link VARCHAR(128),
    keywords VARCHAR(40),
    PRIMARY KEY (`id`)
  );

  CREATE TABLE airports (
    id INT NOT NULL AUTO_INCREMENT,
    csv_id INT,
    ident VARCHAR(8),
    type VARCHAR(20),
    name VARCHAR(80),
    latitude_deg DECIMAL(26, 22),
    longitude_deg DECIMAL(26, 22),
    elevation_ft INT,
    country_id INT NOT NULL,
    iso_region VARCHAR(10),
    municipality VARCHAR(70),
    scheduled_service VARCHAR(8),
    gps_code VARCHAR(8),
    iata_code VARCHAR(8),
    local_code VARCHAR(8),
    home_link VARCHAR(130),
    wikipedia_link VARCHAR(130),
    keywords VARCHAR(180),
    PRIMARY KEY (`id`),
    CONSTRAINT FK_countries_id FOREIGN KEY (country_id) REFERENCES countries (`id`) ON DELETE CASCADE
  );

  CREATE TABLE runways (
    id INT NOT NULL AUTO_INCREMENT,
    airport_id INT NOT NULL,
    surface VARCHAR(60),
    le_ident VARCHAR(8),
    PRIMARY KEY (`id`),
    CONSTRAINT FK_airports_id FOREIGN KEY (airport_id) REFERENCES airports (`id`) ON DELETE CASCADE
  );
  """

  def execute(sql: String, conn: Connection) =
    for (part <- sql.split(";").map(_.trim).filter(_ != ""))
      conn.createStatement.execute(part)

  def str(col: String): String = "\"" + col + "\""

  val database = Databases(
    "com.mysql.jdbc.Driver",
    "jdbc:mysql://localhost:3306/airports?useUnicode=true&characterEncoding=utf-8",
    "default",
    Map("user" -> "root", "password" -> "root")
  )

  //Evolutions.applyEvolutions(database)
  val conn = database.getConnection()

  execute(queryDrop, conn)
  execute(queryCreate, conn)

  val countriesReader = CSVReader.open(new java.io.File("countries.csv"))
  val countriesIter = countriesReader.iterator
  countriesIter.next

  for (cols <- countriesIter) {
    execute(
      "INSERT INTO countries (code, name, continent, wikipedia_link, keywords) " +
      s"VALUES(${str(cols(1))}, ${str(cols(2))}, ${str(cols(3))}, ${str(cols(4))}, ${str(cols(5))})",
      conn
    )
  }
  countriesReader.close()

  val airportsReader = CSVReader.open(new java.io.File("airports.csv"))
  val airportsIter = airportsReader.iterator
  airportsIter.next
  for (cols <- airportsIter) {
    val rs = conn.createStatement.executeQuery(s"SELECT id FROM countries WHERE code = ${str(cols(8))}")
    if (rs.next()) {
      val st = conn.prepareStatement(
        "INSERT INTO airports (csv_id, ident, type, name, latitude_deg, longitude_deg, " +
                              "elevation_ft, country_id, iso_region, municipality, scheduled_service, " +
                              "gps_code, iata_code, local_code, home_link, wikipedia_link, keywords) " +
        s"VALUES(${cols(0)}, ${str(cols(1))}, ${str(cols(2))}, ?, ${cols(4)}, ${cols(5)}, " +
               s"${if (cols(6) == "") "NULL" else cols(6)}, ${rs.getInt("id")}, ${str(cols(9))}, ?, ${str(cols(11))}, " +
               s"${str(cols(12))}, ${str(cols(13))}, ${str(cols(14))}, ${str(cols(15))}, ?, ?)",
      )
      st.setString(1, cols(3))
      st.setString(2, cols(10))
      st.setString(3, cols(16))
      st.setString(4, cols(17))
      st.execute()
    }
    rs.close()
  }
  airportsReader.close()

  val runwaysReader = CSVReader.open(new java.io.File("runways.csv"))
  val runwaysIter = runwaysReader.iterator
  runwaysIter.next
  for (cols <- runwaysIter) {
    val rs = conn.createStatement.executeQuery(s"SELECT id FROM airports WHERE csv_id = ${cols(1)}")
    if (rs.next()) {
      val st = conn.prepareStatement(
        "INSERT INTO runways (airport_id, surface, le_ident) " +
        "VALUES(?, ?, ?)"
      )
      st.setInt(1, rs.getInt("id"))
      st.setString(2, cols(5))
      st.setString(3, cols(8))
      st.execute()
    }
    rs.close()
  }
  runwaysReader.close()

  database.shutdown()

}
