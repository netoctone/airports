# --- !Ups

CREATE TABLE countries (
  id INT NOT NULL AUTO_INCREMENT,
  code VARCHAR(8),
  name VARCHAR(50),
  continent VARCHAR(8),
  wikipedia_link VARCHAR(128),
  keywords VARCHAR(40),
  PRIMARY KEY (`id`)
);

CREATE TABLE airports (
  id INT NOT NULL AUTO_INCREMENT,
  csv_id INT,
  ident VARCHAR(8),
  type VARCHAR(20),
  name VARCHAR(80),
  latitude_deg DECIMAL(26, 22),
  longitude_deg DECIMAL(26, 22),
  elevation_ft INT,
  country_id INT NOT NULL,
  iso_region VARCHAR(10),
  municipality VARCHAR(70),
  scheduled_service VARCHAR(8),
  gps_code VARCHAR(8),
  iata_code VARCHAR(8),
  local_code VARCHAR(8),
  home_link VARCHAR(130),
  wikipedia_link VARCHAR(130),
  keywords VARCHAR(180),
  PRIMARY KEY (`id`),
  CONSTRAINT FK_countries_id FOREIGN KEY (country_id) REFERENCES countries (`id`) ON DELETE CASCADE
);

CREATE TABLE runways (
  id INT NOT NULL AUTO_INCREMENT,
  airport_id INT NOT NULL,
  surface VARCHAR(60),
  le_ident VARCHAR(8),
  PRIMARY KEY (`id`),
  CONSTRAINT FK_airports_id FOREIGN KEY (airport_id) REFERENCES airports (`id`) ON DELETE CASCADE
);

# --- !Downs

DROP TABLE IF EXISTS runways;
DROP TABLE IF EXISTS airports;
DROP TABLE IF EXISTS countries;
